/* This is free and unencumbered software released into the public domain.
 * License: Unlicense
 * For more information, please refer to <http://unlicense.org/>
 */
var BigBase = function () {
	_.Chars = "", _.Table = {}, _.Base = 0;
	// Etc
	function _(aChars) {
		// _.Chars without: [\t\n\r,\\"]
		_.Chars = aChars || "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz !#$%&'()*+-./:;<=>?@[]^_`{|}~";
		_.Table = {}, _.Base = _.Chars.length;
		// Allocate table for speed
		for (var i = 0; i < _.Base; i++)
			_.Table[_.Chars.charAt(i)] = i;
	}
	// Etc
	_.GetString = _.t = function (aNumber, aChars) {
		var retVal = "", charIdx,
			base = _.Base,
			chars = aChars||_.Chars,
			remain = Math.floor(aNumber);
		if (aChars) base = aChars.length;
		while (1) {
			charIdx = remain % base;
			retVal = chars.charAt(charIdx) + retVal;
			remain = Math.floor(remain / base);
			if (!remain)
				break;
		}
		return retVal;
	};
	_.GetNumber = _.f = function (aStr, aChars) {
		var table = _.Table, base = _.Base;
		if (aChars && aChars != _.Chars) {
			base = aChars.length;
			// Reallocate table if needed:
			for (var table = {}, i = 0; i < base; i++)
				table[aChars.charAt(i)] = i;
		}
		for (var retVal = 0, aStr = aStr.split(""), i = 0; i < aStr.length; i++)
			retVal = (retVal * base) + table[aStr[i]];
		return retVal;
	};
	return _(), _;
}();
var GeoIPBasic = (function () {
	function _(aArrOrStr) { _.a(aArrOrStr); }
	_.Country = "AUAustraliaCNChinaJPJapanTHThailandRURussiaINIndiaMYMalaysiaKRSouth KoreaSGSingaporeTWTaiwanHKHong KongKHCambodiaPHPhilippinesVNVietnamNLNetherlandsBRBrazilARArgentinaPSPalestineESSpainFRFranceCZCzechiaATAustriaCHSwitzerlandGBUnited KingdomITItalyGRGreeceDEGermanyUSUnited StatesDKDenmarkPTPortugalSESwedenGHGhanaTRTurkeyCMCameroonZASouth AfricaAEUnited Arab EmiratesPLPolandJOJordanBEBelgiumRORomaniaKEKenyaIEIrelandUGUgandaAMArmeniaTZTanzaniaBIBurundiUYUruguayNONorwayCLChileLULuxembourgFIFinlandBGBulgariaUAUkraineEGEgyptCACanadaILIsraelQAQatarMDMoldovaHRCroatiaIQIraqVAVatican CityCYCyprusVGBritish Virgin IslandsISIcelandGEGeorgiaSKSlovakiaKZKazakhstanEEEstoniaGIGibraltarLVLatviaSISloveniaMXMexicoALAlbaniaHUHungarySASaudi ArabiaLTLithuaniaMTMaltaCRCosta RicaIRIranNZNew ZealandBHBahrainIDIndonesiaCOColombiaSYSyriaLBLebanonOMOmanRSSerbiaMKNorth MacedoniaLILiechtensteinJEJerseyBABosnia and HerzegovinaAZAzerbaijanKGKyrgyzstanRERéunionASAmerican SamoaAIAnguillaAGAntigua and BarbudaAWArubaBSBahamasBBBarbadosBZBelizeBMBermudaBOBoliviaKYCayman IslandsCKCook IslandsCUCubaDMDominicaDODominican RepublicECEcuadorSVEl SalvadorFKFalkland IslandsFJFijiGDGrenadaGPGuadeloupeGUGuamGTGuatemalaGYGuyanaHTHaitiHNHondurasJMJamaicaKIKiribatiMSMontserratNRNauruNCNew CaledoniaNINicaraguaNUNiueNFNorfolk IslandKPNorth KoreaPAPanamaPGPapua New GuineaPYParaguayPEPeruPNPitcairn IslandsPRPuerto RicoKNSt Kitts and NevisLCSaint LuciaVCSt Vincent and GrenadinesWSSamoaSBSolomon IslandsSRSurinameSZEswatiniTKTokelauTOTongaTTTrinidad and TobagoTCTurks and Caicos IslandsTVTuvaluVUVanuatuVEVenezuelaPMSaint Pierre and MiquelonADAndorraAOAngolaBDBangladeshBYBelarusBJBeninBTBhutanBWBotswanaBNBruneiBFBurkina FasoCVCabo VerdeCFCentral African RepublicTDChadCXChristmas IslandCCCocos (Keeling) IslandsKMComorosCDDR CongoCIIvory CoastGQEquatorial GuineaEREritreaETEthiopiaFOFaroe IslandsGAGabonGMGambiaGLGreenlandGWGuinea-BissauGNGuineaKWKuwaitLALaosLSLesothoLRLiberiaLYLibyaMOMacaoMGMadagascarMWMalawiMVMaldivesMLMaliMRMauritaniaMUMauritiusMCMonacoMNMongoliaMEMontenegroMAMoroccoMZMozambiqueMMMyanmarNANamibiaNPNepalNENigerNGNigeriaPKPakistanDJDjiboutiCGCongo RepublicRWRwandaSHSaint HelenaSMSan MarinoSTSão Tomé and PríncipeSNSenegalSLSierra LeoneSOSomaliaLKSri LankaSDSudanSJSvalbard and Jan MayenTJTajikistanTGTogoTNTunisiaTMTurkmenistanUZUzbekistanYEYemenZMZambiaZWZimbabweAFAfghanistanDZAlgeriaAXÅland IslandsIMIsle of ManGGGuernseySCSeychellesAQAntarcticaXKKosovoMPNorthern Mariana IslandsVIU.S. Virgin IslandsUMU.S. Outlying IslandsGFFrench GuianaBLSaint BarthélemyMFSaint MartinMQMartiniquePFFrench PolynesiaWFWallis and FutunaBVBouvet IslandBQBonaireCWCuraçaoEHWestern SaharaFMFederated States of MicronesiaGSSouth Georgia and the South Sandwich IslandsHMHeard and McDonald IslandsIOBritish Indian Ocean TerritoryMHMarshall IslandsPWPalauSSSouth SudanSXSint MaartenTFFrench Southern TerritoriesTLTimor-LesteYTMayotteEUEurope".split(/([A-Z]{2})/);
	_.Object = {};
	_.Ranges = [];
	for (var o = {}, i = 1, ccLen = _.Country.length; i < ccLen; i+=2)
		o[_.Country[0+i]] = _.Country[1+i];
	_.Country = o;
	_.GetMajor = function (aNum) { return (0|aNum/16777216); };
	// etc
	_.Alloc = _.a = function (aArr) {
		var nB64 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz$_";
		for (var ccLen in aArr) {
			var ccode = ccLen.slice(0,2),
				numLen = ccLen.slice(2),
				len   = BigBase.f(numLen, nB64);
			var subArr = aArr[ccLen].split(",");
			//return;
			for (var i in subArr) {
				var sv = 16777216+BigBase.f(subArr[i]),
					ev = sv + len,
					val = [ccode, sv, ev];
				// table allocations:
				var paMin = (0|sv/16777216),
					paMax = 1 + (0|ev/16777216);
				for (var i2 = paMin; i2 < paMax; i2++) {
					if (!_.Object[i2]) _.Object[i2] = [];
					_.Object[i2].push(val);
				}
				_.Ranges.push([sv, ev]);
			}
		}
	};
	_.GetIPv4n = _.ipv4n = function (aIP) {
		var parts = aIP.split(".");
		return parts[0]*16777216 + parts[1]*65536 + parts[2]*256 + +parts[3];
	}
	_.GetIPv4 = _.ipv4 = function (aNum) {
		return (aNum >>> 24 & 0xFF) + '.' +
		(aNum >>> 16 & 0xFF) + '.' +
		(aNum >>> 8 & 0xFF) + '.' +
		(aNum & 0xFF);
	};
	// eg: aCIDR=1.0.0.0/24
	_.DbgCidr = function (aCIDR) {
		return aCIDR = aCIDR.split("/"), [
			aCIDR[0],
			_.GetIPv4(-1 + _.GetIPv4n(aCIDR[0]) + Math.pow(2, 32 - aCIDR[1]))
		];
	};
	// Lookup() is for casual use.
	_.Lookup = _.lookup = _.f = function (aIP) {
		var parts = aIP.split("."),
			nIP = parts[0]*16777216 + parts[1]*65536 + parts[2]*256 + +parts[3],
			pA = +parts[0],
			combos = [
				_.Object[pA], _.Object[1 + pA]
			];
		for (var arr, n = 0; n < 2; n++)
			if (arr = combos[n]) for (var i = 0, fLen = arr.length; i < fLen; i++) {
				var sv = arr[i][1], ev = arr[i][2];
				if (nIP >= sv && nIP <= ev) {
					return [aIP, arr[i][0], _.Country[arr[i][0]], sv, ev];
				}
			}
	};
	return _;
})();
