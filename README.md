# GeoIP Basic
Here it is, free of copyright, free of complex installation process. Just Drag & drop.<br>
<img src="https://api.visitorbadge.io/api/visitors?path=gitlab.ncfoss.geoipjs&countColor=%2337d67a&style=flat-square&labelStyle=upper"><br>

# This is free and unencumbered software released into the public domain.
No rights reserved.<br>

# JS; Example
```js
// Format: [IP, CCode, CName, IPNumStart, IPNumEnd]
// Returns: ["1.0.0.1", "AU", "Australia", 16777216, 16777471]
GeoIPBasic.lookup("1.0.0.1");
// Or use alias:
GeoIPBasic.f("1.0.0.1");
```

# HTML; Load order
```html
<script src="GeoIP-lib.js"></script>
<!-- Then load any of its data file(s): -->
<script src="GeoIP-20230814-data.js"></script>
```

# Recommendation
0. If your server does not gzip encode the JavaScript code, I recommend that it does.<br>
Additionally make exclusion for older browsers that don't support gzip encoding.<br>
1. You can use zip js library to unzip the js data zip.

# Features
0. Small data file 1.64MiB; minimal data.
   - CSV file is ~15MiB, thus 9 times less to load.
1. No modern JavaScript.
2. Simple source code.
3. Decent lookup op/s
4. Made to work on client-side.
5. Should work with many browsers.

# Plans
May just update data files yearly.
